  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  header('Access-Control-Allow-Origin: *');
  ?>
  <!DOCTYPE html>
  <html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pixie</title>


    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url("assets/vendor/fontawesome-free/css/all.min.css");?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url("assets/css/sb-admin-2.min.css");?>" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

      <!-- Sidebar -->
      <?php include('includes/navbar.php'); ?>

      <!-- End of Sidebar -->

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content --> 
        <div id="content">

          <!-- Topbar -->
          <?php include('includes/header.php'); ?>
          <!-- End of Topbar -->

          <!-- Begin Page Content -->

          <!-- Begin Container Fluid -->
          <div class="container-fluid">
            <!-- Begin Page Heading-->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
              <h1 class="h3 mb-0 text-gray-800">Transactions</h1>
            </div>
            <!-- End Page Heading-->


            <!-- DataTables Example -->
            <div class="card shadow mb-4">
             <div class="card-header py-3">
              <div class="row">
                <div class="col-xl-3 col-md-6">
                 <div class="input-group">
                  <input type="text" class="form-control bg-light border-1 small" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary" type="button">
                      <i class="fas fa-search fa-sm"></i>
                    </button>
                  </div>
                </div>
              </div>
              
              <div class="topbar-divider d-none d-sm-block"></div>
             <!-- Filters-->

            </div>
          </div>


          
          <div class="card-body">
            <div class="table-responsive">
              <font size="2">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Ref No</th>
                      <th>Service</th>
                      <th>Channel</th>
                      <th>Amount</th>
                      <th>Transaction date</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>744168597</td>
                      <td>Vortex</td>
                      <td>PayPal</td>
                      <td>100</td>
                      <td>2012/09/26</td>
                      <td>COMPLETED</td>
                    </tr>
                    <tr>
                      <td>744168598</td>
                      <td>Vortex</td>
                      <td>PayPal</td>
                      <td>100</td>
                      <td>2012/09/27</td>
                      <td>COMPLETED</td>
                    </tr>
                    <tr>
                      <td>744168600</td>
                      <td>Vortex</td>
                      <td>SmartPit</td>
                      <td>1000</td>
                      <td>2012/09/27</td>
                      <td>COMPLETED</td>
                    </tr>
                    <tr>
                      <td>744168601</td>
                      <td>Vortex</td>
                      <td>SmartPit</td>
                      <td>50</td>
                      <td>2012/09/27</td>
                      <td>PENDING</td>
                    </tr>
                  </tbody>
                </table>
              </font>
            </div>
          </div>
          <!-- /.card shadow mb-4 -->
        </div>
        <!-- /.container-fluid -->
      </div>
      
      <!-- End of Main Content -->
    </div>
    <!-- Footer -->
    <?php include('includes/footer.php'); ?>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
</div>

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<?php include('logout.php'); ?>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url("assets/vendor/jquery/jquery.min.js");?>"></script>
<script src="<?php echo base_url("assets/vendor/bootstrap/js/bootstrap.bundle.min.js");?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo base_url("assets/vendor/jquery-easing/jquery.easing.min.js");?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo base_url("assets/js/sb-admin-2.min.js");?>"></script>

<!-- Page level plugins -->
<script src="<?php echo base_url("assets/vendor/chart.js/Chart.min.js");?>"></script>

<!-- Page level custom scripts -->
<script src="<?php echo base_url("assets/js/demo/chart-area-demo.js");?>"></script>
<script src="<?php echo base_url("assets/js/demo/chart-pie-demo.js");?>"></script>

</body>
</html>
