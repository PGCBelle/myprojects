<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['dashboard'] = 'homeController';
$route['default_controller'] = 'loginController';
$route['transactions'] = 'transactionsController';
$route['channels'] = 'channelController';
$route['(:any)'] = 'pagesController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
