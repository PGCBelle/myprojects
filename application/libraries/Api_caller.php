<?php

class Api_caller
{

    /** Application Name */
    protected $application_name = APP_NAME ;

    /**
     * Get Environment Variable for WSO2
     *
     * @param environment = input string
     * @return wso2_credentials = output array
     */
   

    /**
     * Token Getter on WSO2 Development Server
     *
     * @param environment = input string
     * @return response = output string
     */
    private function getToken()
    {
        try {

            $curl = curl_init();

            /** wso2 credentials for development server */
            $wso2 = $this->getEnvironmentVariableWSO2();

            $headers[] = "Content-type: application/x-www-form-urlencoded";
            $headers[] = "Authorization: Basic " . base64_encode($wso2['auth']);

            /** Setting options for cURL */
            curl_setopt($curl, CURLOPT_URL, $wso2['token_url'] . '?grant_type=client_credentials'); /** Current setup will look like this: http:test.com:port/ + api_url/1.0.0/ + api_method/version */
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_ENCODING, '');
            curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);
            curl_setopt($curl, CURLOPT_AUTOREFERER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");      

            /** Logging for Token Getting */
            $starting_log = $this->create_starting_logs();
            $this->write_log($starting_log);
            
            $service_name = 'TOKEN GETTER';
            $service_log = $this->create_service_detail_logs($service_name);
            $this->write_log($service_log);

            $ending_log = $this->create_ending_logs();
            $this->write_log($ending_log);

            $response = curl_exec($curl);
            $apidata = json_decode($response);
            curl_close($curl);

        } catch (Exception $e) {
            $apidata = $e;
        }
        // var_dump(json_encode($apidata));

        return $apidata;
    }

    /**
     * cURL FUNCTION TO WSO2 MICROSERVICES REST API
     *
     * @param curl_method = input string: enum [GET, POST, PUT] default: GET
     * @param content_type = input string: enum [XML, JSON]
     * @param api_name = input string
     * @param api_method = input string
     * @param api_url = input string 
     * @param payload = input array (json)
     * @param response = output array
     *
     */ 
    private function api_call($content_type = null, $environment = null, $curl_method = null,  $api_url = null, $api_method = null, $payload = null)
    {

        /** wso2 credentials for development server */
        // $wso2 = $this->getEnvironmentVariableWSO2();

        // $url = $wso2['url'];

        $curl = curl_init();
        $headers = array();

        // $token = (array) $this->getToken();

        $headers[] = ($content_type == 'XML') ? 'Content-Type: text/xml' : 'Content-Type: application/json';
        // $headers[] = 'Authorization: Bearer ' . $token['access_token'];

        /** Setting options for cURL */
        curl_setopt($curl, CURLOPT_URL, BFF_URL . $api_url . $api_method); /** Current setup will look like this: http:test.com:port/ + api_url/1.0.0/ + api_method/version */

        $url_log = $this->create_url_logs(BFF_URL . $api_url . $api_method);
        $this->write_log($url_log);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_ENCODING, '');
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        /** Switch Case for Curl Method */
        switch ($curl_method) {

            case "GET":
                break;

            case "POST":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

                if ($content_type == 'XML') {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
                } else {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($payload));
                }
                break;  
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($payload != null) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($payload));
                }
                break;

            case "PATCH":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
                if ($payload != null) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($payload));
                }
                break;

            default:
                // curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
                break;
        };

        /** Logging for API CALLER */
        $starting_log = $this->create_starting_logs();
        $this->write_log($starting_log);
        
        $service_name = 'API CALLER';
        $service_log = $this->create_service_detail_logs($service_name);
        $this->write_log($service_log);

        $ending_log = $this->create_ending_logs();
        $this->write_log($ending_log);

        /** Logging for Method */
        $starting_log = $this->create_starting_logs();
        $this->write_log($starting_log);
        
        $method_log = $this->create_service_method_logs($api_method);
        $this->write_log($method_log);

        /** Execute the curl and display the response */
        $response = curl_exec($curl);
        /** Get the status code of the request and parse it to integer */
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // private function create_transactionLogs($event_type, $log_service, $log_method, $log_payload, $log_message)

        switch (true) {
            case ($http_status >= 200 && $http_status < 400):
                $log_string = $this->create_transactionLogs($event_type = 'INFO', $log_service =  $log_method = $api_method, $curl_method, $log_payload = $payload, $log_message = $response);
                $this->write_log($log_string);

                $ending_log = $this->create_ending_logs();
                $this->write_log($ending_log);
                break;
            case ($http_status >= 400 && $http_status < 500):
                $log_string = $this->create_transactionLogs($event_type = 'WARN', $log_service =  $log_method = $api_method, $curl_method, $log_payload = $payload, $log_message = $response);
                $this->write_log($log_string);

                $ending_log = $this->create_ending_logs();
                $this->write_log($ending_log);
                break;
            case ($http_status >= 500 && $http_status < 599):
                $log_string = $this->create_transactionLogs($event_type = 'ERR', $log_service =  $log_method = $api_method, $curl_method, $log_payload = $payload, $log_message = $response);
                $this->write_log($log_string);

                $ending_log = $this->create_ending_logs();
                $this->write_log($ending_log);
                break;
            case (ENVIRONMENT === 'debug'):
                $log_string = $this->create_transactionLogs($event_type = 'DEBUG', $log_service =  $log_method = $api_method, $curl_method, $log_payload = $payload, $log_message = $response);
                $this->write_log($log_string);
                break;
            default:
                break;
        }

        if (!isset($_SESSION)) {
            session_start();
        }

        curl_close($curl);
        return $response;

    }

    /**
     * Create client request id
     *
     * @return clientRequestId = output string
     */
    private function create_clientRequestId()
    {
        /** Work around for unique id for clientRequestID */
        $microtime = microtime();
        $arr_microtime = explode(" ", $microtime);
        $milisec = str_replace(".", "", $arr_microtime[0]);

        $clientRequestId = CLIENT_REQUEST_PREFIX . "" . date("Ymdhis") . $milisec;
        return $clientRequestId;
    }

    /**
     * Create Transaction Logs
     *
     * @param event_type = input string - enum [INFO, WARN, ERROR, DEBUG]
     * @param service = input string
     * @param method = input string
     * @param payload = input strings
     * @param message = input string (json)
     * @return log = output string
     *
     */
    private function create_transactionLogs($event_type, $log_service, $log_method, $log_payload, $log_message)
    {
        /** Timestamp Format: ISO-8601 */
        $timestamp = date('c');

        $log = "\n";
        $log .= $timestamp;
        $log .= ' [' . $event_type . ']';
        $log .= ', app=' . $this->application_name;
        $log .= ', service=' . $log_service;
        $log .= ', method=' . $log_method;
        $log .= ', payload=' . json_encode($log_payload);
        $log .= ', message=' . $log_message;
        $log .= ', requestId=' . $this->create_clientRequestId();

        return $log;
    }

    private function create_starting_logs() { 
        $timestamp = date('c');
        
        $log = "\n";
        $log .= $timestamp;
        $log .= ' [STARTING LOG]';

        return $log;
    }

    private function create_url_logs($url) { 
        $timestamp = date('c');
        
        $log = "\n";
        $log .= $timestamp;
        $log .= ' [URL: '.$url.']';

        return $log;
    }

    private function create_service_detail_logs($service_name) { 
        $timestamp = date('c');
        
        $log = "\n";
        $log .= $timestamp;
        $log .= ' [SERVICE: '.$service_name.']';

        return $log;
    }

    private function create_service_method_logs($service_method) { 
        $timestamp = date('c');
        
        $log = "\n";
        $log .= $timestamp;
        $log .= ' [METHOD: '.$service_method.']';

        return $log;
    }

    private function create_ending_logs() { 
        $timestamp = date('c');
        
        $log = "\n";
        $log .= $timestamp;
        $log .= ' [ENDING LOG]';

        return $log;
    }

    /**
     * Write Transaction/Error Logs
     *
     * @param message = input string
     */
    private function write_log($message)
    {
        /** new service path for microservice logs */
        $file_path = APPPATH . "/logs/BFF_call_" . date('Ymd', time()) . ".php";

        /** Execute this if there's no existing log file like $file_path */
        if (!file_exists($file_path)) {
            /** First line of the log */
            $line_msg = "<?php if (! defined('BASEPATH')) exit('No direct script access allowed'); ?>\n";
            
            /** Create new file for the logs */
            write_file($file_path, $line_msg);
        }
        /** Append message onto the log file */
        write_file($file_path, $message, 'a');
    }

    /**
     * List of WSO2 Microservice API
     */

    /**
     * Referral Microservice
     *
     * Procedure:
     *  -> create referee (POST)
     *  -> create referer (POST)
     *  -> create reward (POST)
     *
     * Optional Procedure:
     *  -> get all reward (GET)
     *  -> get reward (POST)
     *  -> inquire referee (POST)
     *  -> inquire referer (POST)
     */

    /** START - Referral Microservice */

    /**
     * Create Referee
     *
     * @param payload = input string (json)
     * @return api_return = output array
     */
    public function send_request( $curl_method, $api_url, $api_method, $payload)
    {
        // $api_url = '/bene-referral';
        // $api_method = '/createReferee';
        // $curl_method ='POST'
        $content_type = 'JSON';

        $api_return = $this->api_call($content_type, ENVIRONMENT, $curl_method, $api_url, $api_method, $payload);

        return json_decode($api_return, true);
    }


    /**
     * Sample Post: Create Referer
     *
     * @param payload = input string (json)
     * @return api_return = output array
     */
    public function referral_createReferee($payload = null)
    {
        $api_name = 'Referral';
        $api_url = '/bene-referral';
        $api_method = '/createReferee';
        $content_type = 'JSON';
        $environment = 'PROD';

        $curl_method = 'POST';
        $api_return = $this->api_call($content_type, $environment, $curl_method, $api_name, $api_url, $api_method, $payload);

        return json_decode($api_return, true);
    }

    
    /**
     * Sample put: Create Reward
     *
     * @param payload = input string (json)
     * @return api_return = output array
     */
    public function referral_createReward($payload = null)
    {
        $api_name = 'Referral';
        $api_url = '/bene-referral';
        $api_method = '/createReward';
        $content_type = 'JSON';
        $environment = 'PROD';

        $curl_method = 'PUT';
        $api_return = $this->api_call($content_type, $environment, $curl_method, $api_name, $api_url, $api_method, $payload);

        return json_decode($api_return, true);
    }

    /**
     * Sample get: Get All Reward
     *
     * @return api_return = output array
     */
    public function referral_getAllReward()
    {
        $api_name = 'Referral';
        $api_url = '/bene-referral';
        $api_method = '/getAllReward';
        $content_type = 'JSON';
        $environment = 'PROD';

        $curl_method = 'GET';
        $api_return = $this->api_call($content_type, $environment, $curl_method, $api_name, $api_url, $api_method); /** Didn't include the payload here because it wasn't needed for GET method */

        return json_decode($api_return, true);
    }

    /**
     * Sample patch: Get All Reward
     *
     * @return api_return = output array
     */
    public function campaign_updateCampaign($payload = null, $param = null)
    {
        $api_name = 'Campaign';
        $api_url = '/bene-campaign';
        $api_method = '/updateCampaign'. '/' . $param;
        $content_type = 'JSON';
        $environment = 'PROD';

        $curl_method = 'PATCH';
        $api_return = $this->api_call($content_type, $environment, $curl_method, $api_name, $api_url, $api_method, $payload);

        return json_decode($api_return, true);
    }

   
}
